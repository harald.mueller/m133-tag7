-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 15. Feb 2019 um 00:30
-- Server-Version: 10.1.30-MariaDB
-- PHP-Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `haraldmueller`
--
use haraldmueller;
-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `t_abteilungen`
--

CREATE TABLE `t_abteilungen` (
  `a_id` int(11) UNSIGNED NOT NULL,
  `a_kuerzel` varchar(50) NOT NULL,
  `a_name` varchar(50) NOT NULL,
  `a_local_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `t_abteilungen`
--

INSERT INTO `t_abteilungen` (`a_id`, `a_kuerzel`, `a_name`, `a_local_id`) VALUES
(1, 'SALE', 'Verkauf', 2),
(2, 'ENTW', 'Entwicklung', 3),
(3, 'PROD', 'Produktion', 3),
(4, 'BUCH', 'Buchhaltung', 2),
(5, 'GLEI', 'Geschaeftsleitung', 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `t_personen`
--

CREATE TABLE `t_personen` (
  `p_id` int(11) UNSIGNED NOT NULL,
  `p_vorname` varchar(50) NOT NULL,
  `p_nachname` varchar(50) NOT NULL,
  `p_email` varchar(50) NOT NULL,
  `p_loginpasswort` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `t_personen`
--

INSERT INTO `t_personen` (`p_id`, `p_vorname`, `p_nachname`, `p_email`, `p_loginpasswort`) VALUES
(1, 'Harald', 'Mueller', 'harald.mueller@tbz.ch', 'keinpasswort'),
(2, 'Matthias', 'von Orelli', 'matthias.vonorelli@tbz.ch', 'keinpasswort'),
(3, 'Hansi', 'Hinterseher', 'hansi.hinterseher@tbz.ch', 'keinpasswort'),
(4, 'Florian', 'Silbereisen', 'florian.silbereisen@tbz.ch', 'keinpasswort'),
(5, 'Helene', 'Fischer', 'helene.fischer@tbz.ch', 'keinpasswort'),
(6, 'Beatrice', 'Egli', 'beatrice.egli@tbz.ch', 'keinpasswort'),
(7, 'Nemo', 'Mettler', 'nemo.mettler@tbz.ch', 'keinpasswort'),
(8, 'Andrea', 'Berg', 'andrea.berg@tbz.ch', 'keinpasswort');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `t_abteilungen`
--
ALTER TABLE `t_abteilungen`
  ADD PRIMARY KEY (`a_id`),
  ADD UNIQUE KEY `a_id` (`a_id`);

--
-- Indizes für die Tabelle `t_personen`
--
ALTER TABLE `t_personen`
  ADD PRIMARY KEY (`p_id`),
  ADD UNIQUE KEY `p_id` (`p_id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `t_abteilungen`
--
ALTER TABLE `t_abteilungen`
  MODIFY `a_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT für Tabelle `t_personen`
--
ALTER TABLE `t_personen`
  MODIFY `p_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
